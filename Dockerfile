FROM debian:stable-slim
MAINTAINER Timothy Redaelli <timothy.redaelli@gmail.com>

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get -o Acquire::Retires=10 install --no-install-recommends -y \
    ca-certificates curl gnupg && \
    printf 'deb http://deb.torproject.org/torproject.org stable main' >> /etc/apt/sources.list && \
    curl https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | apt-key add - && \
    apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -o Acquire::Retires=10 install --no-install-recommends -y \
    libcap2-bin \
    rinetd \
    tor && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN chown debian-tor /etc/tor/torrc && \
    setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/tor && \
    printf '\n%s\n' 'SocksPort 0.0.0.0:9050' 'ControlPort 9051' 'DNSPort 0.0.0.0:53' >> /etc/tor/torrc && \
    printf '0.0.0.0\t9052\t127.0.0.1\t9051\n' >> /etc/rinetd.conf

COPY startup.sh /usr/local/bin/startup.sh

USER debian-tor
CMD /usr/local/bin/startup.sh
